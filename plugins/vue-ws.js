import Vue from "vue";
import ws from "./ws.js";

let VueWebsocket = {};

VueWebsocket.install = function(Vue, url) {
  Vue.prototype.$socket = ws(url);
};

export default ({ store }, inject) => {
  Vue.use(VueWebsocket, "wss://dev.profitplay.com/websocket/ws/connect");
};
