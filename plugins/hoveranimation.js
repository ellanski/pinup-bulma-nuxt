import Vue from "vue";
import throttle from "lodash/throttle";

let HoverAnimation = {};

const defaultValue = {
  animation: "hover-animation",
  duration: 1000
};

// TODO а может быть REQUESTANIMATIONFRAME?

HoverAnimation.install = function(Vue) {
  Vue.directive("hoveranimation", {
    inserted(el, binding) {
      binding.value = binding.value || defaultValue;
      let { animation, duration } = binding.value;

      let playAnimation = throttle(
        function() {
          el.classList.add("animated");
          setTimeout(function() {
            el.classList.remove("animated");
          }, duration);
        },
        duration,
        { trailing: false }
      );

      el.classList.add(animation);
      el.addEventListener("mouseover", playAnimation);
    }
  });
};

Vue.use(HoverAnimation);
