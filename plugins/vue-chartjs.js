import Vue from "vue";
import { Line } from "vue-chartjs";
import 'chartjs-plugin-zoom';
// import 'chartjs-plugin-streaming';

Vue.component("my-line", {
  extends: Line,
  props: ["data", "options"],
  methods: {
    update() {
      this.$data._chart.update();
    }
  },
  mounted() {
    this.renderChart(this.data, this.options);
  }
});
