import Vue from "vue";
import VueNativeSock from "vue-native-websocket";


export default ({ store }, inject) => {
  Vue.use(VueNativeSock, "wss://dev.profitplay.com/websocket/ws/connect", {
    store: store,
    // format: 'json',
    passToStoreHandler: function (eventName, event) {
        let msg = event
        eventName = eventName.toUpperCase()

        if (eventName === 'SOCKET_ONMESSAGE') {
            const msg = JSON.parse(event.data)
            switch(msg.command) {
                case 'quotes-last':
                    this.store.commit('updateQuotes', msg.data.quotes)
                    break
                default:
                    console.log('Какая-то хуйня')
            }
        }

        if (eventName === 'SOCKET_ONOPEN') {
            this.store.commit('SOCKET_ONOPEN')
        }
    }
  });
};
