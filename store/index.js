import Vue from "vue";
import Vuex from "vuex";

const MAX_QUOTES_LENGTH = 600;

Vue.use(Vuex);

const store = () =>
  new Vuex.Store({
    state: {
      activeTool: "",
      currentTime: "",
      timeframe: "",
      socket: {
        isConnected: false
      },
      user: {
        name: "nameimpobable",
        vip: true,
        balance: 631,
        cashback: 3500,
        currency: "rub",
        language: "RU",
        lang: ""
      },
      deals: {
        currentBet: 100,
        currentExpiration: 60,
        bets: [10, 50, 100, 500, 1000, 2000, 5000],
        oneClickDeal: false,
        betprogress: 0,
        history: []
      },

      quotes: [{ time: "", quotes: {} }],
      activeToolQuotes: [],
      events: [],
      timeframes: [],
      openDeals: [],
      closedDeals: [],
      tools: [],
      news: [],

      translations: {
        EXPIRATION: "Временной интервал:",
        BET: "Сумма сделки:",
        BUTTON_UP: "Вверх",
        CURRENT_QUOTES: "Текущий курс:",
        BUTTON_DOWN: "Вниз",
        LEFT_NAV_DEALS: "Сделки",
        LEFT_NAV_TOURNAMENTS: "Турниры",
        LEFT_NAV_NEWS: "Новости",
        LEFT_NAV_EVENTS: "Календарь",
        LEFT_NAV_TUTORIALS: "Обучение",
        ACTIVE_DEALS: "Активные сделки",
        BET_HISTORY: "История торговли",
        NO_ACTIVE_DEALS: "У Вас нет ни одной активной сделки",
        ERROR: "Ошибка",
        CREATE_OPTION_2: "Сумма ставки не входит в разрешенный диапазон",
        CREATE_OPTION_4:
          "Выберите меньшее время экспирации. Торги по инструменту закроются раньше времени экспирации.",
        CREATE_OPTION_5: "Превышено количество открытых позиций.",
        CREATE_OPTION_6:
          "Недостаточно средств для открытия опциона. Пополните свой счет или потренируйтесь на бесплатном демо-счете.",
        CREATE_OPTION_7: "Котировка изменилась. Купить по новой цене?",
        CREATE_OPTION_8: "Превышен лимит максимального выиграша.",
        CREATE_OPTION_11: "Превышен суммарный объем открытых позиций.",
        CREATE_OPTION_12: "Превышен объём ставок по инструменту.",
        CREATE_OPTION_17: "ВИ - недоступен",
        CREATE_OPTION_18: "Достигнут лимит безрисковых ставок.",
        ТUTORIAL_START: "Начать",
        ТUTORIAL_PREV: "Назад",
        ТUTORIAL_NEXT: "Продолжить",
        ТUTORIAL_FINISH: "Закончить",
        TUTORIAL_STEP_1:
          "<p>Выберите актив для прогноза</p><small>Актив — акции компаний, сырьё и валютные пары</small>",
        TUTORIAL_STEP_2:
          "<p>Выберите сумму сделки, например 100 рублей</p><small>Под блоком выбора суммы указан % прибыли и Ваш доход при верном прогнозе</small>",
        TUTORIAL_STEP_3:
          "<p>Выберите продолжительность сделки, например 30 секунд</p><small>Время экспирации составляет от 30 секунд до суток</small>",
        TUTORIAL_STEP_4:
          "<p>Проанализируйте график, чтобы заработать на прогнозе движения вверх или вниз</p>",
        TUTORIAL_STEP_5: "<p>Выберите направление движения графика</p>",
        TUTORIAL_STEP_6:
          "<p>Следите за текущим состоянием Вашего прогноза в разделе «Активные сделки»</p>",
        TOOL_UNAVAILABLE: "Торговля по данному инструменту закрыта"
      }
    },

    getters: {
      quoteById: state => id => {
        return state.quotes[state.quotes.length - 1].quotes[id];
      },
      isToolOpen: state => id => {
        const tool = state.tools.find(tool => {
          return tool.id === id
        })
        return tool && tool.open
      }
    },

    mutations: {
      SET_SOCKET_READY(state) {
        state.socket.isConnected = true;
      },
      SET_EVENTS(state, payload) {
        state.events = payload;
      },
      SET_NEWS(state, payload) {
        state.news = payload;
      },
      SET_TIMEFRAMES(state, payload) {
        state.timeframes = payload;
      },
      SET_CURRENT_TIMEFRAME(state, payload) {
        state.timeframe = payload;
      },
      SET_OPEN_DEALS(state, payload) {
        state.openDeals = payload;
      },
      SET_CLOSED_DEALS(state, payload) {
        state.closedDeals = payload;
      },
      SET_TOOLS(state, [tools, tabs]) {
        state.tools = tools.filter(tool => {
          return tabs.indexOf(tool.id) >= 0;
        });
      },
      SET_ACTIVE_TOOL(state, tool) {
        state.activeTool = tool;
      },
      ADD_OPEN_DEAL(state, deal) {
        state.openDeals.push(deal);
      },
      SET_ACTIVE_TOOL_QUOTES(state, payload) {
        console.log(payload.slice(-10));
        state.activeToolQuotes = payload
          .slice(-MAX_QUOTES_LENGTH)
          .map(quote => {
            return { time: quote.time, quotes: quote.quotes };
          });
      },
      CLOSE_OPTION(state, payload) {
        const dealToClose = state.openDeals.find(deal => {
          return deal.id === payload.id;
        });
        const dealToCloseIndex = state.openDeals.indexOf(dealToClose);

        if (dealToCloseIndex > -1) {
          state.openDeals.splice(dealToCloseIndex, 1);
        }
        state.closedDeals.unshift(payload);
      },

      updateCurrentTime(state, payload) {
        state.currentTime = payload;
      },
      updateBalance(state, payload) {
        state.user.balance += payload.bet * payload.rate;
      },
      incrementBetprogress(state) {
        state.deals.betprogress++;
      },
      toggleOneClickDeal(state) {
        state.deals.oneClickDeal = !state.deals.oneClickDeal;
      },
      addDealToHistory(state, payload) {
        state.deals.history.unshift(payload);
      },
      changeCurrentBet(state, payload) {
        state.deals.currentBet = payload;
      },

      updateQuotes(state, { time, quotes }) {
        if (state.quotes[state.quotes.length - 1].time === time) {
          state.quotes[state.quotes.length - 1].time = time;
          state.quotes[state.quotes.length - 1].quotes = quotes;
        } else {
          if (state.quotes.length > MAX_QUOTES_LENGTH) state.quotes.shift();
          state.quotes.push({ time, quotes });
        }
      }
    },

    actions: {
      GET_ALL({ commit, state }, response) {
        commit("SET_EVENTS", response.events);
        commit("SET_NEWS", response.news);
        commit("SET_TIMEFRAMES", response.timeframes);
        commit("SET_OPEN_DEALS", response.openDeals);
        commit("SET_CLOSED_DEALS", response.closedOptions.data);
        commit("SET_TOOLS", [response.tools, response.user.tabs]);
        commit("SET_ACTIVE_TOOL", response.tools[0].id);

        Vue.moment.locale(state.user.lang || "ru");
      }
    }
  });

export default store;
