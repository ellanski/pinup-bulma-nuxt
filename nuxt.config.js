module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "{{ name }}",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "{{escape description }}"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },

  plugins: [
    {src: '~plugins/vue-ws.js', ssr: false},
    {src: '~plugins/hoveranimation.js', ssr: false},
    // {src: '~plugins/vue-timers.js', ssr: false},
    {src: '~plugins/vue-moment.js', ssr: false},
    // {src: '~plugins/native-websocket.js', ssr: false},
    {src: '~plugins/perfect-scrollbar.js', ssr: false},
    // {src: '~plugins/vue-chartjs.js', ssr: false},
    {src: '~plugins/vue-tour.js', ssr: false}
  ],

  css: [
    '~/assets/main.scss',
    '~/assets/hover-animations.scss',
    '~/assets/buefy-custom.scss',
    '~/assets/flags.css',
    '~/assets/fonts/style.scss'
  ],

  modules: [
    "nuxt-buefy",
    // '@nuxtjs/axios',
    // '@nuxtjs/auth',
    ['nuxt-sass-resources-loader', '~assets/_variables.scss']
  ],
  /*
  ** Build configuration
  */
  build: {
    analyze: true,
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
